package com.demo.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UsersList
 */
@WebServlet("/usersList")
public class UsersList extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Connection con;
	public void init() {
		try {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		 con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle","hr","hr");
	}catch(Exception e) {
		e.printStackTrace();
	}
	}
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsersList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	PrintWriter out=response.getWriter();
	response.setContentType("text/html");
	try {
	PreparedStatement ps=con.prepareStatement("select * from users");
	ResultSet rs=ps.executeQuery();
	ResultSetMetaData rsmd=rs.getMetaData();
	int count=rsmd.getColumnCount();
	out.println("<table border=1>");
	if(!rs.next()) {
		out.println("No users exist!");
		out.println("<a href=register.html>Register here</a>");
	}else {
	do {
		out.println("<tr>");
		for(int i=1;i<=count;i++) {
			out.println("<td>"+rs.getString(i)+"</td>");
			}
		out.println("</tr>");
	}while(rs.next());
	out.println("</table>");
	}
	}catch(Exception e) {
		e.printStackTrace();
	
	}
	
	
	

}
}
